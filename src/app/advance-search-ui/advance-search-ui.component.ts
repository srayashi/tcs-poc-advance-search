import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { debounceTime } from 'rxjs/operators';
import { ApiService } from '../service/api.service';

@Component({
  selector: 'app-advance-search-ui',
  templateUrl: './advance-search-ui.component.html',
  styleUrls: ['./advance-search-ui.component.css']
})
export class AdvanceSearchUiComponent implements OnInit {
  list = [
    {
      'id': 17800222,
      'organization': 'ICG',
      'trasaction': 94621357,
      'business': 'China-Consumer',
      'sub': 'BIVA(Benes inmmuebla y Valores Adjustment)',
      'region': 'LATAM(including BANAMAX)',
      'country': 'Australia(AU)',
      'type': 'Real Estate',
      'status': 'Extention Expired'
    },
    {
      'id': 17800222,
      'organization': 'GLOBAL CONSUMER',
      'trasaction': 94621357,
      'business': 'China-Consumer',
      'sub': 'BIVA(Benes inmmuebla y Valores Adjustment)',
      'region': 'LATAM(including BANAMAX)',
      'country': 'Australia(AU)',
      'type': 'Real Estate',
      'status': 'Extention Expired'
    },
    {
      'id': 17800222,
      'organization': 'ICG',
      'trasaction': 94621357,
      'business': 'China-Consumer',
      'sub': 'BIVA(Benes inmmuebla y Valores Adjustment)',
      'region': 'LATAM(including BANAMAX)',
      'country': 'Australia(AU)',
      'type': 'Real Estate',
      'status': 'Extention Expired'
    },
    {
      'id': 17800222,
      'organization': 'GLOBAL CONSUMER',
      'trasaction': 94621357,
      'business': 'China-Consumer',
      'sub': 'BIVA(Benes inmmuebla y Valores Adjustment)',
      'region': 'LATAM(including BANAMAX)',
      'country': 'Australia(AU)',
      'type': 'Real Estate',
      'status': 'Extention Expired'
    },
    {
      'id': 17800222,
      'organization': 'ICG',
      'trasaction': 94621357,
      'business': 'China-Consumer',
      'sub': 'BIVA(Benes inmmuebla y Valores Adjustment)',
      'region': 'LATAM(including BANAMAX)',
      'country': 'Australia(AU)',
      'type': 'Real Estate',
      'status': 'Extention Expired'
    },
    {
      'id': 17800222,
      'organization': 'GLOBAL CONSUMER',
      'trasaction': 94621357,
      'business': 'China-Consumer',
      'sub': 'BIVA(Benes inmmuebla y Valores Adjustment)',
      'region': 'LATAM(including BANAMAX)',
      'country': 'Australia(AU)',
      'type': 'Real Estate',
      'status': 'Extention Expired'
    },
    {
      'id': 17800222,
      'organization': 'ICG',
      'trasaction': 94621357,
      'business': 'China-Consumer',
      'sub': 'BIVA(Benes inmmuebla y Valores Adjustment)',
      'region': 'LATAM(including BANAMAX)',
      'country': 'Australia(AU)',
      'type': 'Real Estate',
      'status': 'Extention Expired'
    },
    {
      'id': 17800222,
      'organization': 'GLOBAL CONSUMER',
      'trasaction': 94621357,
      'business': 'China-Consumer',
      'sub': 'BIVA(Benes inmmuebla y Valores Adjustment)',
      'region': 'LATAM(including BANAMAX)',
      'country': 'Australia(AU)',
      'type': 'Real Estate',
      'status': 'Extention Expired'
    },
    {
      'id': 17800222,
      'organization': 'ICG',
      'trasaction': 94621357,
      'business': 'China-Consumer',
      'sub': 'BIVA(Benes inmmuebla y Valores Adjustment)',
      'region': 'LATAM(including BANAMAX)',
      'country': 'Australia(AU)',
      'type': 'Real Estate',
      'status': 'Extention Expired'
    },
    {
      'id': 17800222,
      'organization': 'GLOBAL CONSUMER',
      'trasaction': 94621357,
      'business': 'China-Consumer',
      'sub': 'BIVA(Benes inmmuebla y Valores Adjustment)',
      'region': 'LATAM(including BANAMAX)',
      'country': 'Australia(AU)',
      'type': 'Real Estate',
      'status': 'Extention Expired'
    },
    {
      'id': 17800222,
      'organization': 'ICG',
      'trasaction': 94621357,
      'business': 'China-Consumer',
      'sub': 'BIVA(Benes inmmuebla y Valores Adjustment)',
      'region': 'LATAM(including BANAMAX)',
      'country': 'Australia(AU)',
      'type': 'Real Estate',
      'status': 'Extention Expired'
    },
    {
      'id': 17800222,
      'organization': 'GLOBAL CONSUMER',
      'trasaction': 94621357,
      'business': 'China-Consumer',
      'sub': 'BIVA(Benes inmmuebla y Valores Adjustment)',
      'region': 'LATAM(including BANAMAX)',
      'country': 'Australia(AU)',
      'type': 'Real Estate',
      'status': 'Extention Expired'
    }
  ]
  id = new FormControl('');
  organization = new FormControl('');
  transaction = new FormControl('');
  country = new FormControl('');
  region = new FormControl('');
  type = new FormControl('');
  status = new FormControl('');
  business = new FormControl('');
  sub = new FormControl('');
  debouncedValue = '';
  searchObj = {
    id: '',
    organization: '',
    transaction: '',
    country: '',
    region: '',
    type: '',
    status: '',
    business: '',
    sub: '',
  }
  errMsg = '';
  constructor(private apiService: ApiService) { }

  ngOnInit(): void {
    this.id.valueChanges
    .pipe(debounceTime(1500))
    .subscribe((value) => {
      this.searchObj.id = value;
      if(this.searchObj.id.length >= 3 || this.searchObj.id.length === 0) {
        this.getSearchResult();
      }
    });
    this.organization.valueChanges
    .pipe(debounceTime(1500))
    .subscribe((value) => {
      this.searchObj.organization = value;
      if(this.searchObj.organization.length >= 3 || this.searchObj.organization.length === 0) {
        this.getSearchResult();
      }
    });
    this.transaction.valueChanges
    .pipe(debounceTime(1500))
    .subscribe((value) => {
      this.searchObj.transaction = value;
      if(this.searchObj.transaction.length >= 3 || this.searchObj.transaction.length === 0) {
        this.getSearchResult();
      }
    });
    // this.transaction.valueChanges
    // .pipe(debounceTime(500))
    // .subscribe((value) => {
    //   this.searchObj.transaction = value;
    //     this.getSearchResult();
    // });
  }
  getSearchResult(){
    console.log('aaa', this.searchObj);
    // this.apiService.searchApi(this.searchName, this.searchStatus, this.searchGender).subscribe((res:any)=> {
    //   this.list = res.results;
    //   console.log('res', this.list)
    // },(err) => {
    //   this.list = [];
    //   this.errMsg = err.error.error;
    //   console.log(err.error.error)
    // })
  }
}
