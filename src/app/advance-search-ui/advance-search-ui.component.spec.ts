import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ApiService } from '../service/api.service';
import { HttpClientTestingModule } from "@angular/common/http/testing";
import { AdvanceSearchUiComponent } from './advance-search-ui.component';

describe('AdvanceSearchUiComponent', () => {
  let component: AdvanceSearchUiComponent;
  let fixture: ComponentFixture<AdvanceSearchUiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      declarations: [ AdvanceSearchUiComponent ],
      providers : [
        ApiService
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdvanceSearchUiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
