import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private httpClient: HttpClient) { }
  searchApi(name:string,status:string,gender:string) {
    return this.httpClient.get(`https://rickandmortyapi.com/api/character/?name=${name}&status=${status}&gender=${gender}`);
  }
  getApi() {
    return this.httpClient.get(`https://rickandmortyapi.com/api/character/`)
  }
}
