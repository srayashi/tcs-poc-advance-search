import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { debounceTime } from 'rxjs/operators';
import { ApiService } from '../service/api.service';

@Component({
  selector: 'app-advance-search',
  templateUrl: './advance-search.component.html',
  styleUrls: ['./advance-search.component.css']
})
export class AdvanceSearchComponent implements OnInit {
  name = new FormControl();
  status = new FormControl();
  gender = new FormControl('');
  debouncedValue = '';
  searchName = '';
  searchStatus = '';
  searchGender = '';
  list = [];
  errMsg = '';
  genders = ['Male','Female'];
  constructor(private apiService: ApiService) { }

  ngOnInit() {
    this.apiService.getApi().subscribe((res:any)=> {
      console.log(res)
      this.list = res.results;
    })
    this.name.valueChanges
    .pipe(debounceTime(1500))
    .subscribe((value) => {
      this.searchName = value;
      if(this.searchName.length >= 3 || this.searchName.length === 0) {
        this.getSearchResult();
      }
    });
    this.status.valueChanges
    .pipe(debounceTime(1500))
    .subscribe((value) => {
      this.searchStatus = value;
      if(this.searchStatus.length >= 3 || this.searchStatus.length === 0) {
        this.getSearchResult();
      }
    });
    this.gender.valueChanges
    .pipe(debounceTime(500))
    .subscribe((value) => {
      this.searchGender = value;
        this.getSearchResult();
    });
  }
  getSearchResult(){
    this.apiService.searchApi(this.searchName, this.searchStatus, this.searchGender).subscribe((res:any)=> {
      this.list = res.results;
      console.log('res', this.list)
    },(err) => {
      this.list = [];
      this.errMsg = err.error.error;
      console.log(err.error.error)
    })
  }
}
