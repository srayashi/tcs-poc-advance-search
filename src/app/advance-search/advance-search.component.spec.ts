import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ApiService } from '../service/api.service';
import { HttpClientTestingModule } from "@angular/common/http/testing";
import { AdvanceSearchComponent } from './advance-search.component';

describe('AdvanceSearchComponent', () => {
  let component: AdvanceSearchComponent;
  let fixture: ComponentFixture<AdvanceSearchComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      declarations: [ AdvanceSearchComponent ],
      providers : [
        ApiService
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdvanceSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  // it('should check user email is invalid', () => {
  //   let name = component.name;
  //   expect(name).toBeTruthy();
  // });
});
